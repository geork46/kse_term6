#include <stdexcept>
#include <cmath>
#include <cassert>
#include "math_objects.h"

namespace math_objects
{

Vector operator * (Matrix const & m, Vector const & v)
{
	assert(m.dimension() == v.dimension());
	size_t n = m.dimension();
	Vector res(n);	
	for (size_t i = 0; i < n; ++i)
	{
		double sum = 0;
		for (size_t j = 0; j < n; ++j)
			sum += m[i][j] * v[j];
		res[i] = sum;
	}
	return res;
}

double Vector::norm() const
{
	double abs = 0;
	for (int i = 0; i < data.size(); ++i)
		abs += data[i] * data[i];
	return sqrt(abs);
}

double Matrix::norm() const
{
	double abs = 0;
	for (int i = 0; i < data.size(); ++i)
		abs += data[i] * data[i];
	return sqrt(abs);
}

Vector operator + (Vector const & a, Vector const & b)
{
	assert(a.dimension() == b.dimension());
	size_t n = a.dimension();
	Vector res(n);
	for (size_t i = 0; i < n; ++i)
		res[i] = a[i] + b[i];
	return res;
}

Vector operator - (Vector const & a, Vector const & b)
{
	assert(a.dimension() == b.dimension());
	size_t n = a.dimension();
	Vector res(n);
	for (size_t i = 0; i < n; ++i)
		res[i] = a[i] - b[i];
	return res;
}

double& Vector::operator [](size_t i)
{
	assert(i < data.size());
	return data[i];
}

double const& Vector::operator [](size_t i) const
{
	assert(i < data.size());
	return data[i];
}
double* Matrix::operator [](size_t i)
{
	if (i >= n)
		throw std::logic_error("Vector lndex Out Of Bounds");
	return &data[i * n];
}

double const* Matrix::operator [](size_t i) const
{
	if (i >= n)
		throw std::logic_error("Vector lndex Out Of Bounds");
	return &data[i * n];
}

Matrix::Matrix(size_t n) : n(n), data(n * n, 0) { }

Matrix::Matrix(size_t n, Type type) : n(n), data(n * n, 0) 
{
	switch (type)
	{
		case IDENTITY:
			for (size_t i = 0; i < n; ++i)
				data[n * i + i] = 1;
			break;
		case HILBERT:
			for (size_t i = 0; i < n; ++i)
				for (size_t j = 0; j < n; ++j)
					data[i * n + j] = 1 / (1 + i + j);
			break;
		case DIAGONAL_DOMINANT:
			for (size_t i = 0; i < n; ++i)
				for (size_t j = 0; j < n; ++j)
					data[i * n + j] = (i == j) ? n : 1;
			break;
	}
}

std::istream& operator >> (std::istream & in, Vector & v)
{
	for (size_t i = 0; i < v.data.size(); ++i)
		in >> v.data[i];
	return in;
}

std::ostream& operator << (std::ostream & out, Vector const & v)
{
	for (size_t i = 0; i < v.data.size(); ++i)
		out << v.data[i] << " ";
	return out;
}

std::istream& operator >> (std::istream & in, Matrix & m)
{
	for (size_t i = 0; i < m.data.size(); ++i)
		in >> m.data[i];
	return in;
}

std::ostream& operator << (std::ostream & out, Matrix const & m)
{
	for (size_t i = 0; i < m.n; ++i)
	{
		for (size_t j = 0; j < m.n; ++j)
			out << m.data[i * m.n + j] << " ";
		out << std::endl;
	}
	return out;
}

Vector::Vector(size_t n) : data(n, 0) { }

Vector::Vector(std::vector<double> const &v) : data(v) { }

size_t Vector::dimension() const
{
	return data.size();
}

size_t Matrix::dimension() const
{
	return n;
}
}

