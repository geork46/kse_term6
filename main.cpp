#include "jacobi.h" 

using namespace math_objects;
using namespace std;

int main(int argc, char* argv[])
{
	freopen("input.txt", "r", stdin);
	size_t n;
	cin >> n;
	Matrix a(n, Matrix::IDENTITY);
	Vector b(n);
	cin >> b;//a >> b;	
	Jacobi alg(a, b);
	b = alg.startMethod(0.001);
	cout << b;
	return 0;
}
