#include "jacobi.h"
#include <cassert>

Jacobi::Jacobi(Matrix const & m, Vector const & v) : b(m.dimension()), g(v.dimension()), x(v.dimension())
{
	assert(m.dimension() == v.dimension());
	// TODO Check zeros on the diagonal

	for (size_t	i = 0; i < b.dimension(); ++i)
	{
		for (size_t j = 0; j < b.dimension(); ++j)
			if (i == j)
				b[i][j] = 0;
			else
				b[i][j] = m[i][j] / m[i][i]; 
		g[i] = v[i] / m[i][i];
		x[i] = v[i] / m[i][i];
	}	
}

Vector Jacobi::startMethod(double e)
{
	assert(e > 0);
	assert(b.norm() <= 1);
	Vector dx(b.dimension()), tx(b.dimension());
	do
	{
		tx = b * x + g;
		dx = x - tx;
		x = tx;	
	} while (dx.norm() >= e);
	return x;
}
