#ifndef _MATH_OBJECTS_h_
#define _MATH_OBJECTS_h_

#include <cstdio>
#include <vector>
#include <iostream>

namespace math_objects
{

class Vector
{
public:
	Vector(size_t n); 
	Vector(std::vector<double> const &v);
	double& operator[] (size_t i);
	double norm() const;
	double const& operator[] (size_t i) const;
	size_t dimension() const;
private:
	std::vector<double> data;
	friend std::istream& operator >> (std::istream &, Vector &);
	friend std::ostream& operator << (std::ostream &, Vector const &);
};

class Matrix
{
public:
	enum Type {IDENTITY, HILBERT, DIAGONAL_DOMINANT};
	Matrix(size_t n);
	Matrix(size_t n, Type type);
	size_t dimension() const;
	double norm() const;
	double* operator[] (size_t i);
	double const* operator[] (size_t i) const;
private:
	std::vector<double> data;
	size_t n;
	friend std::istream& operator >> (std::istream &, Matrix &);
	friend std::ostream& operator << (std::ostream &, Matrix const &);
};

std::istream& operator >> (std::istream &, Vector &);
std::ostream& operator << (std::ostream &, Vector const &);
std::istream& operator >> (std::istream &, Matrix &);
std::ostream& operator << (std::ostream &, Matrix const &);

Vector operator * (Matrix const &, Vector const &);
Vector operator + (Vector const &, Vector const &);
Vector operator - (Vector const &, Vector const &);
}

#endif
