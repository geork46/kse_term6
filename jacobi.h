#ifndef _JACOBI_H_
#define _JACOBI_H_
#include "math_objects.h"

using namespace math_objects;

class Jacobi
{
public:
	Jacobi(Matrix const &, Vector const &);
	Vector startMethod(double e);	
private:
	Matrix b;	// x = b * x + g
	Vector g, x;
	double e;
};

#endif
